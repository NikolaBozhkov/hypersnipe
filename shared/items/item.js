export class Item {
	constructor(hashName, type, meta) {
		this.hashName = hashName;
		this.type = type;
		this.name = meta.name;
		this.iconUrl = meta.iconUrl;
		this.nameColor = meta.nameColor;
		this.qualityColor = meta.qualityColor;
		this.price = meta.price;
	}
}
