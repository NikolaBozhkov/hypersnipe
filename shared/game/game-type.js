function getGameType(playersCount, entryTickets, prizePoolMin, prizePoolMax) {
    return {
        playersCount,
        entryTickets,
        prizePoolMin,
        prizePoolMax
    }
}

export let GameType = Object.freeze({
    // small: getGameType(5, 20, 900, 1800),
    big: getGameType(10, 10, 1000, 1500)
});
