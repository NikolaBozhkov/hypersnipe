export const constants = {
	itemSellRatio: 0.9,
	blueprintUpgradeRatio: 0.4,
	blueprintSellRatio: 0.5
};

export const itemType = {
	skinBlueprint: 'skinBlueprint',
	skinTicket: 'skinTicket',
	material: 'material',
	raw: 'raw'
};

export let itemsUtil = {
	getSellPrice(type, price) {
		if (type == itemType.skinBlueprint) {
			return price * constants.blueprintSellRatio;
		} else {
			return price * constants.itemSellRatio;
		}
	}
};
