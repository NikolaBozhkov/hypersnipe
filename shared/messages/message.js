export class Message {
	constructor(username, avatarUrl, text) {
		this.username = username;
		this.avatarUrl = avatarUrl;
		this.text = text;
		this.time = Date.now();
	}
}
