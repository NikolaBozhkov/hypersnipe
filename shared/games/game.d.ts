import { Player } from './player.d';

export class Game {
    id: string;
    type: number;
    round: number;
    startTime: Date;
    players: [Player];
    bets: [object];
	
	constructor(id: string, type: number);
}
