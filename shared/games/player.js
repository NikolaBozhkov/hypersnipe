export class Player {
	constructor(steamId, steamName, avatarUrl) {
        this.steamId = steamId;
        this.steamName = steamName;
        this.avatarUrl = avatarUrl;
        this.points = 0;
        this.currentBet;
	}
}