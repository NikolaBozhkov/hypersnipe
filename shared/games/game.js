export class Game {
	constructor(id, type) {
        this.id = id;
        this.type = type;
        this.round = 0;
        this.startTime = Date.now();
        this.players = [];
        this.bets = [];
	}
}