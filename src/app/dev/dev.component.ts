import { Component, OnInit } from '@angular/core';
import { NgbTypeahead } from '@ng-bootstrap/ng-bootstrap';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';

import { InventoryService } from '../inventory/inventory.service';
import { GameService } from '../game/game.service';
import { QueueService } from '../queue/queue.service';
import { itemType } from '../../../shared/util';
import { Item } from './item';

@Component({
    selector: 'app-dev',
    templateUrl: './dev.component.html',
    styleUrls: ['./dev.component.scss'],
    providers: [InventoryService]
})
export class DevComponent implements OnInit {

    item = new Item('', '');
    skins: string[] = [];
    inventory: Item[] = [];

    search = (text$: Observable<string>) =>
    text$
      .distinctUntilChanged()
      .map(term => term.length < 2 ? []
        : this.skins.filter(v => v.toLowerCase().indexOf(term.toLowerCase()) > -1).slice(0, 20));

    constructor(private inventoryService: InventoryService,
        private gameService: GameService,
        private queueService: QueueService) { }

    async ngOnInit() {
        this.item.type = itemType.skinTicket;
        this.loadSkins();
        this.loadInventory();
    }

    async loadSkins() {
        this.skins = await this.inventoryService.getAllItems();
    }

    loadInventory() {
        this.inventoryService.getInventory().then(inv => {
            this.inventory = inv.map(x => new Item(x.name, x.type));
        });
    }

    async add() {
        try {
            let skin = await this.inventoryService.addNewItem(this.item);
            this.inventory.push(skin);
        } catch (e) {
            console.log(e);
        }
    }

    async remove(item: Item) {
        try {
            let removed = await this.inventoryService.removeItem(item);
            console.log(removed);
            let index = this.inventory.findIndex(i =>
                i.name == removed.name
                && i.type == removed.type
            );

            console.log(this.inventory);
            if (index > -1) {
                this.inventory.splice(index, 1);
            }
        } catch (e) {
            console.log(e);
        }
    }

    toggleItemType() {
        if (this.item.type == itemType.skinTicket) {
            this.item.type = itemType.skinBlueprint;
        } else {
            this.item.type = itemType.skinTicket;
        }
    }

    async leaveGame() {
        try {
            let didLeave = await this.gameService.leaveGame();
            console.log(didLeave);
        }
        catch (err) {
            console.log(err);
        }
    }

    async leaveQueue() {
        try {
            let didLeave = await this.queueService.leaveQueue();
            console.log(didLeave);
        }
        catch (err) {
            console.log(err);
        }
    }

    async skipQueue() {
        this.gameService.skipQueue();
    }
}
