export class Message {
    username: String;
	avatarUrl: String;
	text: String;
	time: Date;
}
