import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Message } from './message';

@Injectable()
export class MessagesService {

    constructor(private http: HttpClient) {
    }

    getAllMessages(): Promise<Message[]> {
        return this.http.get<Message[]>('api/messages').toPromise();
	}

	sendMessage(text: string) {
        this.http.post('api/messages', { text }).toPromise();
	}
}
