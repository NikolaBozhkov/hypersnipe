import { Component, OnInit } from '@angular/core';
import { MessagesService } from './messages.service';
import { SocketService } from '../socket/socket.service';
import { Message } from './message';

@Component({
    selector: 'app-chat',
    templateUrl: './chat.component.html',
    styleUrls: ['./chat.component.scss'],
    providers: [MessagesService]
})
export class ChatComponent implements OnInit {

    messages: Message[] = [];

    constructor(private messagesService: MessagesService, private socketService: SocketService) { }

    ngOnInit() {
        this.getMessages();
        this.socketService.onNewMessage().subscribe(message => {
            this.messages.push(message);
        });
    }

    async getMessages() {
        let messages = await this.messagesService.getAllMessages();

        if (!messages) {
            // DO sth to say it failed
            console.log('failed to load messages');
            return;
        }

        this.messages = messages;
    }

    sendMessage(message: string) {
        this.messagesService.sendMessage(message);
    }
}
