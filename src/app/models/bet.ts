import { Player } from './player';

export class Bet {
    multiplier: number;
    winAmount: number;

    constructor(public from: number, public to: number, public amount: number, public playerId: string) {
        this.multiplier = 100 / (to - from);
        this.winAmount = this.multiplier * amount;
    }
}
