import { Bet } from './bet';

export class Player {
    constructor(
        public id: string,
        public steamName: string,
        public avatarUrl: string,
        public color: string,
        public coins: number) {
    }
}
