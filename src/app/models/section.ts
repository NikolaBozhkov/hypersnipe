export class Section {
    highlighted: boolean;
    color: string;

    constructor(public from: number, public to: number, public amount: number) {
        this.highlighted = false;
    }
}
