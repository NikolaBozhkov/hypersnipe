import { itemType, constants } from '../../../shared/util.js';
import { Item } from './item';

export class Skin extends Item {

	shortName: string;
	wear: string;
	statTrak: boolean;
	blueprintUpgradeCost: number;

	constructor(
		private hashName: string,
		name: string,
		public type: string,
		public iconUrl: string,
		public nameColor: string,
		public qualityColor: string,
		public price: number) {

		super(name, type, price, 1);

		let { shortName, wear, statTrak } = this.breakName(name);
		this.shortName = shortName;
		this.wear = wear;
		this.statTrak = statTrak;

		if (type == itemType.skinBlueprint) {
            this.blueprintUpgradeCost = price * constants.blueprintUpgradeRatio;
        } else {
            this.blueprintUpgradeCost = undefined;
        }
	}

	private breakName(name: string) {
		// let shortName = name.replace('\u2605 ', '');
		let shortName = name;
		let statTrakString = 'StatTrak\u2122';
		let statTrakIndex = name.indexOf(statTrakString);

		let statTrak = false;
		if (statTrakIndex != -1) {
			statTrak = true;
			shortName = shortName.replace(statTrakString + ' ', '');
		}

		let wearMatch = /\((.*)\)/.exec(name);
		let wear = '';
		if (wearMatch && wearMatch[1]) {
			wear = wearMatch[1];
			shortName = shortName.replace(` (${wear})`, '');
		}

		return { shortName, wear, statTrak };
	}
}
