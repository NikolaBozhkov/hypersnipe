import { itemsUtil, constants, itemType } from '../../../shared/util.js';

export class Item {

    sellPrice: number;

    constructor(public name: string, public type: string, public price: number, public quantity: number) {
        this.sellPrice = itemsUtil.getSellPrice(type, price);
    }
}
