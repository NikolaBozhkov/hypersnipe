import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Observer } from 'rxjs/Observer';
import { SocketEvent } from './socket-event';
import { UserService } from '../user/user.service';
import { Message } from '../chat/message';
import { BetResponse } from '../game/bet.response';
import { GameReadyResponse } from '../queue/ready-check/game-ready.response';

import * as socketIo from 'socket.io-client';

const SERVER_URL = 'http://localhost:3000';

@Injectable()
export class SocketService {
    private socket;
    onlineUsersCount: number;

    constructor(private userService: UserService) { }

    public initSocket(): void {
        this.socket = socketIo(SERVER_URL);
        this.socket.on(SocketEvent.CONNECT, async () => {
            try {
                await this.userService.loadData();
                this.socket.emit('userId', { userId: this.userService.id });
            }
            catch (e) {
                console.log(e);
            }
        });

        this.socket.on('onlineUsersCount', onlineUsersCount => {
            this.onlineUsersCount = onlineUsersCount;
        });
    }

    public onNewMessage(): Observable<Message> {
        return new Observable<Message>(observer => {
            this.socket.on('newMessage', (message: Message) =>  {
                observer.next(message);
            });
        });
    }

    public onJoinedGame(): Observable<string> {
        return new Observable<string>(observer => {
            this.socket.on('joinedGame', data => {
                observer.next(data.gameId);
            });
        })
    }

    public onNewBet(): Observable<BetResponse> {
        return new Observable<BetResponse>(observer => {
            this.socket.on('newBet', bet => {
                observer.next(bet);
            });
        })
    }

    public onGameReady(): Observable<GameReadyResponse> {
        return new Observable<GameReadyResponse>(observer => {
            this.socket.on('gameReady', gameReadyRes => {
                observer.next(gameReadyRes);
            })
        })
    }

    public onUserAccepted(): Observable<void> {
        return new Observable<void>(observer => {
            this.socket.on('userAccepted', () => {
                observer.next();
            });
        })
    }

    public onUserDeclined(): Observable<void> {
        return new Observable<void>(observer => {
            this.socket.on('userDeclined', () => {
                observer.next();
            });
        })
    }
}
