import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AuthGuard } from './auth/auth.guard';
import { MainLayoutComponent } from './main-layout/main-layout.component';

import { LandingComponent } from './landing/landing.component';
import { InventoryComponent } from './inventory/inventory.component';
import { DevComponent } from './dev/dev.component';
import { RangeGameComponent } from './range-game/range-game.component';

const routes: Routes = [
    {
        path: '',
        component: MainLayoutComponent,
        canActivate: [AuthGuard],
        children: [
            {
                path: 'inventory',
                component: InventoryComponent
            },
            {
                path: 'dev',
                component: DevComponent
            },
            {
                path: 'games/:id',
                component: RangeGameComponent
            },
            {
                path: '',
                pathMatch: 'full',
                redirectTo: 'inventory'
            }
        ]
    },
    {
        path: 'login',
        component: LandingComponent
    },
    { path: '**', redirectTo: '' }
]

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }
