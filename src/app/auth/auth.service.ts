import { Injectable } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';

@Injectable()
export class AuthService {

    constructor(private cookieService: CookieService) { }

    get isAuthenticated(): boolean {
        return this.cookieService.check('token');
    }

    login() {
		window.location.href = 'api/auth/login';
	}
}
