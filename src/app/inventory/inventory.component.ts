import { Component, OnInit } from '@angular/core';
import { Item } from '../models/item';
import { Skin } from '../models/skin';
import { constants, itemsUtil, itemType } from '../../../shared/util.js';
import { InventoryService } from './inventory.service';

@Component({
    selector: 'app-inventory',
    templateUrl: './inventory.component.html',
    styleUrls: ['./inventory.component.scss'],
    providers: [InventoryService]
})
export class InventoryComponent implements OnInit {
    itemsUtil = itemsUtil;
    itemType = itemType;
    skins: Skin[];
    newItem: any;
    itemsForge: Item[] = [];
    testItems: number[] = [];

    constructor(private inventoryService: InventoryService) { }

    ngOnInit() {
        this.loadAllItems();
    }

    addToForge(item: Skin) {
        let index = this.skins.indexOf(item);
        if (index != -1) this.skins.splice(index, 1);
        this.itemsForge.push(item);
    }

    removeFromForge(item: Skin) {
        let index = this.itemsForge.indexOf(item);
        if (index != -1) this.itemsForge.splice(index, 1);
        this.skins.push(item);
    }

    // async forgeItems() {
    //     let item = await this.inventoryService.forgeItems(this.itemsForge);
    //     if (item === undefined) return;
    //
    //     this.itemsForge = [];
    //     this.items.push(item);
    // }
    //
    // async addNewItem() {
    //     await this.inventoryService.addNewItem(this.newItem);
    //     this.loadAllItems();
    // }

    async loadAllItems() {
        this.skins = await this.inventoryService.getInventory();
    }

    async sellItem(item: Item) {
        // let data = await this.inventoryService.sellItem(item);
        // this.loadAllItems();
    }
}
