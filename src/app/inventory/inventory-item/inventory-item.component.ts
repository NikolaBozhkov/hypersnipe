import { Component, OnInit, Input, HostListener, ViewChild, ElementRef, Renderer2  } from '@angular/core';
import { Skin } from '../../models/skin';
import { itemType } from '../../../../shared/util';

@Component({
    selector: 'app-inventory-item',
    templateUrl: './inventory-item.component.html',
    styleUrls: ['./inventory-item.component.scss']
})
export class InventoryItemComponent implements OnInit {

    @Input() item: Skin;
    @ViewChild('itemInfo') itemInfo: ElementRef;
    @ViewChild('sideIcons') sideIcons: ElementRef;
    isExpanded = false;

    constructor(private renderer: Renderer2, private elRef: ElementRef) { }

    ngOnInit() {
    }

    @HostListener('click')
    showInfo() {
        let itemHtml = this.elRef.nativeElement as HTMLElement;
        let itemInfoHtml = this.itemInfo.nativeElement as HTMLElement;
        let extraWidth = (itemInfoHtml.offsetWidth - itemHtml.offsetWidth) / 2;
        let extraHeight = itemInfoHtml.offsetHeight - itemHtml.offsetHeight;

        this.isExpanded = true;

        let inventory = this.renderer.parentNode(this.elRef.nativeElement) as HTMLElement;

        let mainContainer = document.getElementById('main-content');

        if (itemHtml.offsetLeft - extraWidth < 0) {
            this.renderer.addClass(itemInfoHtml, 'shift-right');
            this.renderer.removeClass(itemInfoHtml, 'shift-left');
        } else if (itemHtml.offsetLeft + itemHtml.offsetWidth + extraWidth > mainContainer.offsetWidth) {
            this.renderer.addClass(itemInfoHtml, 'shift-left');
            this.renderer.removeClass(itemInfoHtml, 'shift-right');
        }

        if (itemHtml.offsetTop + itemHtml.offsetWidth + extraHeight > mainContainer.offsetHeight) {
            this.renderer.addClass(itemInfoHtml, 'shift-top');
        } else {
            this.renderer.removeClass(itemInfoHtml, 'shift-top');
        }
    }

    @HostListener('mouseleave')
    hideInfo() {
        this.isExpanded = false;
        this.toggleSideIcons(false);
    }

    @HostListener('mouseenter')
    moveSideIcons() {
        this.toggleSideIcons(true);
    }

    getWearClass() {
        switch (this.item.wear) {
            case 'Battle-Scarred':
                return 'bs';
            case 'Well-Worn':
                return 'ww';
            case 'Field-Tested':
                return 'ft';
            case 'Minimal Wear':
                return 'mw';
            case 'Factory New':
                return 'fn';
            default:
                break;
        }
    }

    getSkinType() {
        if (this.item.type == itemType.skinBlueprint) {
            return 'blueprint';
        } else if (this.item.type == itemType.skinTicket) {
            return 'ticket';
        }
    }

    toggleSideIcons(hovered: boolean) {
        if (hovered) {
            this.renderer.setStyle(this.sideIcons.nativeElement,
                'bottom', -this.sideIcons.nativeElement.offsetHeight / 2 + 'px');
        } else {
            this.renderer.setStyle(this.sideIcons.nativeElement,
                'bottom', '0');
        }
    }
}
