import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UserService } from '../user/user.service';
import { Skin } from '../models/skin';
import { Item } from '../dev/item';

@Injectable()
export class InventoryService {
    constructor(private http: HttpClient, private userService: UserService) {}

    getInventory(): Promise<Skin[]> {
        return this.http.get<SkinResponse[]>('api/inventory').toPromise()
            .then(inventory => {
                return inventory.map(item => new Skin(item.hashName, item.name, item.type, item.iconUrl, item.nameColor, item.qualityColor, item.price))
            });
    }

    addNewItem(item: Item): Promise<Item> {
        return this.http.post<Item>('api/inventory/addItem', { item }).toPromise();
    }

    removeItem(item: Item): Promise<Item> {
        return this.http.post<Item>('api/inventory/removeItem', { item }).toPromise();
    }

    //
    getAllItems(): Promise<string[]> {
        return this.http.get<string[]>('api/inventory/allitems').toPromise();
    }
    //
    // sellItem(item: Item): Promise<any> {
    //     return this.httpClient
    //         .fetch('inventory/sell', {
    //             method: 'PUT',
    //             body: JSON.stringify({
    //                 name: item.name,
    //                 type: item.type
    //             })
    //         })
    //         .then(res => res.json())
    //         .then(data => {
    //             this.accountService.skinCurrency += data.sellPrice;
    //             console.log(data);
    //             return data;
    //         });
    // }
    //
    // forgeItems(items: Item[]): Promise<Item> {
    //     return this.httpClient
    //         .fetch('inventory/forge', {
    //             method: 'PUT',
    //             body: JSON.stringify({ items })
    //         })
    //         .then(res => res.json());
    // }
}
