interface SkinResponse {
    hashName: string;
    type: string;
    name: string;
    iconUrl: string;
    nameColor: string;
    qualityColor: string;
    price: number;
}
