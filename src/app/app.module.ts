import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CookieService } from 'ngx-cookie-service';
import { AppRoutingModule } from './app-routing.module';

import { MainLayoutComponent } from './main-layout/main-layout.component';
import { AppComponent } from './app.component';
import { AuthGuard } from './auth/auth.guard';
import { RangeSliderComponent } from './range-game/range-slider/range-slider.component';

import { AuthService } from './auth/auth.service';
import { UtilService } from './util/util.service';
import { UserService } from './user/user.service';
import { GameService } from './game/game.service';
import { QueueService } from './queue/queue.service';
import { SocketService } from './socket/socket.service';

import { LandingComponent } from './landing/landing.component';
import { HudComponent } from './main-layout/hud/hud.component';
import { InventoryComponent } from './inventory/inventory.component';
import { ChatComponent } from './chat/chat.component';
import { DevComponent } from './dev/dev.component';
import { RangeGameComponent } from './range-game/range-game.component';
import { InventoryItemComponent } from './inventory/inventory-item/inventory-item.component';
import { GameBarChartComponent } from './range-game/game-bar-chart/game-bar-chart.component';
import { GameSectionsTableComponent } from './range-game/game-sections-table/game-sections-table.component';
import { PlayComponent } from './queue/play/play.component';
import { ReadyCheckComponent } from './queue/ready-check/ready-check.component';

@NgModule({
    declarations: [
        AppComponent,
        LandingComponent,
        MainLayoutComponent,
        HudComponent,
        InventoryComponent,
        ChatComponent,
        InventoryItemComponent,
        DevComponent,
        RangeGameComponent,
        GameBarChartComponent,
        GameSectionsTableComponent,
        RangeSliderComponent,
        PlayComponent,
        ReadyCheckComponent
    ],
    imports: [
        CommonModule,
        HttpClientModule,
        BrowserModule,
        FormsModule,
        AppRoutingModule,
        NgbModule.forRoot()
    ],
    providers: [CookieService, AuthGuard, AuthService, UtilService, UserService, GameService, QueueService, SocketService],
    bootstrap: [AppComponent]
})
export class AppModule { }
