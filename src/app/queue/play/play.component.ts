import { Component, OnInit } from '@angular/core';
import { Observable, Subscription } from 'rxjs/Rx';

import { GameType } from '../../../../shared/game/game-type';
import { UserService } from '../../user/user.service';
import { QueueService } from '../../queue/queue.service';
import { SocketService } from '../../socket/socket.service';

interface GameType {
    playersCount: number;
    entryTickets: number;
    prizePoolMin: number;
    prizePoolMax: number;
}

@Component({
    selector: 'app-play',
    templateUrl: './play.component.html',
    styleUrls: ['./play.component.scss'],
    providers: [QueueService]
})
export class PlayComponent implements OnInit {

    isGamePickerActive = false;
    timeInQueue: number = 0;
    isInQueue: boolean;
    didFindGame: boolean;
    timeSubscription: Subscription;
    gameTypes = Object.values(GameType) as GameType[];

    constructor(private userService: UserService,
        private socketService: SocketService,
        private queueService: QueueService) { }

    ngOnInit() {
        this.socketService.onGameReady().subscribe(() => {
            this.leaveQueue();
            this.didFindGame = true;
            this.isGamePickerActive = false;
        });
    }

    async joinQueue(gameType: GameType) {
        this.didFindGame = false;
        // TODO: Validate tickets and everything
        if (this.userService.tickets < gameType.entryTickets) {
            // TODO: Alert not enough tickets

            // return;
        }

        try {
            this.isInQueue = true; // Rarely gonna be different
            this.isInQueue = await this.queueService.joinQueue(gameType);
            if (this.didFindGame) {
                this.isInQueue = false;
            }

            if (this.isInQueue) {
                this.timeSubscription = Observable.timer(0, 1000).subscribe(t => this.timeInQueue = t);
            }
        }
        catch (err) {
            this.isInQueue = false;
            console.log(err);
        }
    }

    async leaveQueue() {
        try {
            this.isInQueue = false; // Rarely fails so give it immediate response
            this.isInQueue = await !this.queueService.leaveQueue();

            if (!this.isInQueue && !this.didFindGame) {
                this.timeSubscription.unsubscribe();
                this.timeInQueue = 0;
            }
        }
        catch (err) {
            console.log(err);
        }
    }
}
