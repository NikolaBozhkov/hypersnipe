import { Injectable, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { GameType } from '../game/game-type';

@Injectable()
export class QueueService {

    constructor(private http: HttpClient) { }

    joinQueue(gameType: GameType): Promise<boolean> {
        return this.http.post<boolean>('api/queue', { gameType }).toPromise();
    }

    leaveQueue(): Promise<boolean> {
        return this.http.post<boolean>('api/queue/cancel', {}).toPromise();
    }

    acceptGame(readyCheckId: string): Promise<any> {
        return this.http.post<boolean>('/api/queue/accept', { readyCheckId }).toPromise();
    }

    declineGame(readyCheckId: string): Promise<boolean> {
        return this.http.post<boolean>('/api/queue/decline', { readyCheckId }).toPromise();
    }
}
