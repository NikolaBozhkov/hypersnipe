import { Component, OnInit } from '@angular/core';

import { SocketService } from '../../socket/socket.service';
import { QueueService } from '../queue.service';

import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';

@Component({
    selector: 'app-ready-check',
    templateUrl: './ready-check.component.html',
    styleUrls: ['./ready-check.component.scss']
})
export class ReadyCheckComponent implements OnInit {
    totalCount: number;
    acceptedCount: number;
    readyCheckId: string;
    active = false;
    timerDecline: any;

    constructor(private socketService: SocketService,
        private queueService: QueueService) { }

    ngOnInit() {
        this.socketService.onGameReady().subscribe(gameReadyRes => {
            this.active = true;
            this.acceptedCount = 0;
            this.totalCount = gameReadyRes.playersCount;
            this.readyCheckId = gameReadyRes.readyCheckId;
            this.timerDecline = setTimeout(() => {
                this.decline();
            }, 20000);
        });

        this.socketService.onUserAccepted().subscribe(() => {
            this.acceptedCount += 1;
        });

        this.socketService.onJoinedGame().subscribe(() => {
            this.active = false;
        });

        this.socketService.onUserDeclined().subscribe(() => {
            this.active = false;
        })
    }

    accept() {
        clearTimeout(this.timerDecline);
        this.queueService.acceptGame(this.readyCheckId);
    }

    async decline() {
        try {
            clearTimeout(this.timerDecline);
            await this.queueService.declineGame(this.readyCheckId);
        }
        catch (e) {
            console.log(e);
        }
    }
}
