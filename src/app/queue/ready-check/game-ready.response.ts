export interface GameReadyResponse {
    playersCount: number;
    readyCheckId: string;
}
