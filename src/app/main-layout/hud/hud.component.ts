import { Component, OnInit } from '@angular/core';
import { UserService } from '../../user/user.service';

@Component({
    selector: 'app-hud',
    templateUrl: './hud.component.html',
    styleUrls: ['./hud.component.scss']
})
export class HudComponent implements OnInit {

    constructor(private userService: UserService) { }

    ngOnInit() {
    }

}
