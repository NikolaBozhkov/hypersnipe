import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Router, NavigationEnd, RouterEvent } from '@angular/router';

import { DevComponent } from '../dev/dev.component';
import { InventoryComponent } from '../inventory/inventory.component';
import { RangeGameComponent } from '../range-game/range-game.component';
import { ReadyCheckComponent } from '../queue/ready-check/ready-check.component';

@Component({
    selector: 'app-main-layout',
    templateUrl: './main-layout.component.html',
    styleUrls: ['./main-layout.component.scss']
})
export class MainLayoutComponent implements OnInit {

    @ViewChild('menuItems') menuItems: ElementRef;
    @ViewChild('inventoryLink') inventoryLink: ElementRef;
    @ViewChild('devLink') devLink: ElementRef;
    @ViewChild('accountLink') accountLink: ElementRef;
    @ViewChild('marketplaceLink') marketplaceLink: ElementRef;
    @ViewChild('gamesLink') gamesLink: ElementRef;
    isChatHidden: boolean;
    sliderLeft = 0;
    sliderWidth = 0;
    activeHtml: HTMLElement;

    constructor(private router: Router) { }

    ngOnInit() {
        this.isChatHidden = true;

        let assignActive = (url: string) => {
            if (url.indexOf('inventory') > -1) {
                this.activeHtml = this.inventoryLink.nativeElement as HTMLElement;
            }
            else if (url.indexOf('games') > -1) {
                this.activeHtml = this.gamesLink.nativeElement as HTMLElement;
            }
            else if (url.indexOf('dev') > -1) {
                this.activeHtml = this.devLink.nativeElement as HTMLElement;
            }
        }

        assignActive(this.router.url);

        this.router.events.subscribe(event => {
            if (event instanceof NavigationEnd) {
                assignActive(event.urlAfterRedirects);
            }
        });
    }
}
