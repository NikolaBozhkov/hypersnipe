export interface PlayerResponse {
    id: string;
    steamName: string;
    avatarUrl: string;
    coins: number;
    won: number;
    lost: number;
}
