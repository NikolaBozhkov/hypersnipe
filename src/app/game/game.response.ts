import { PlayerResponse } from './player.response';
import { BetResponse } from './bet.response';

export interface GameResponse {
    gameId: string;
    players: PlayerResponse[];
    currentRoundBets: BetResponse[];
}
