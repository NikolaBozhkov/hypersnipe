export interface GameType {
    playersCount: number,
    entryTickets: number,
    prizePoolMin: number,
    prizePoolMax: number
}
