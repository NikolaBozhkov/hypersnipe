export interface BetResponse {
    from: number;
    to: number;
    amount: number;
    multiplier: number;
    winAmount: number;
    playerId: string;
}
