import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { GameResponse } from './game.response';
import { BetResponse } from './bet.response';

@Injectable()
export class GameService {

    constructor(private http: HttpClient) { }

    placeBet(gameId: string, from: number, to: number, amount: number): Promise<BetResponse> {
        return this.http.post<BetResponse>(`api/games/${gameId}/bet`, { gameId, from, to, amount }).toPromise();
    }

    getGame(gameId: string): Promise<GameResponse> {
        return this.http.get<GameResponse>(`api/games/${gameId}`).toPromise();
    }

    // Dev
    leaveGame(): Promise<boolean> {
        return this.http.post<boolean>('api/games/leave', {}).toPromise();
    }

    skipQueue(): Promise<boolean> {
        console.log(' skip req');
        return this.http.post<boolean>('api/queue/skip', {}).toPromise();
    }
}
