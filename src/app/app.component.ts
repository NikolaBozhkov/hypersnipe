import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SocketService } from './socket/socket.service';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

    constructor(private router: Router, private socketService: SocketService) {}

    async ngOnInit() {
        try {
            this.socketService.initSocket();
            this.socketService.onJoinedGame().subscribe(gameId => {
                this.router.navigateByUrl('games/'+ gameId);
            });
        }
        catch (e) {
            console.log(e);
        }
    }
}
