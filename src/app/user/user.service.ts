import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Subject } from 'rxjs/Subject';
import { UserResponse } from './user.response';

@Injectable()
export class UserService {
    id: string;
    steamName: string;
    avatarUrl: string;
    tickets: number = 0;
	skinCurrency: number = 0;
	ticketCurrency: number = 0;

    isServiceReady = false;

    constructor(private http: HttpClient) {
    }

    async loadData(): Promise<boolean> {
        if (this.isServiceReady) {
            return Promise.resolve(true);
        }

        // Doesn't prevent multiple querying of data until loaded
        try {
            let user = await this.http.get<UserResponse>('/api/user').toPromise();
            this.id = user._id;
            this.steamName = user.steamName;
            this.avatarUrl = user.avatarUrl;
            this.tickets = user.tickets;
            this.skinCurrency = user.skinCurrency;
            this.ticketCurrency = user.ticketCurrency;
            this.isServiceReady = true;

            return Promise.resolve(true);
        }
        catch (err) {
            console.log(err);
            return Promise.reject('Could not get user.');
        }
    }
}
