export interface UserResponse {
    _id: string;
    steamName: string;
    avatarUrl: string;
    tickets: number;
    skinCurrency: number;
    ticketCurrency: number;
}
