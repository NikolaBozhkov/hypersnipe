import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Bet } from '../models/bet';
import { Player } from '../models/player';
import { Section } from '../models/section';
import { UserService } from '../user/user.service';
import { GameService } from '../game/game.service';
import { SocketService } from '../socket/socket.service';

const colorsHex = [
    '#ff0303',
    '#0042ff',
    '#1ce6b9',
    '#540081',
    '#fffc01',
    '#fe8a0e',
    '#20c000',
    '#e55bb0',
    '#959697',
    '#7ebff1',
    '#106246',
    '#4e2a04',
];

const colorsRgba = [
    'rgba(255, 3, 3, 0.5)',
    'rgba(0, 66, 255, 0.5)',
    'rgba(28, 230, 185, 0.5)',
    'rgba(84, 0, 129, 0.5)',
    'rgba(255, 252, 1, 0.5)',
    'rgba(254, 138, 14, 0.5)',
    'rgba(32, 192, 0, 0.5)',
    'rgba(229, 91, 176, 0.5)',
    'rgba(149, 150, 151, 0.5)',
    'rgba(126, 191, 241, 0.5)',
    'rgba(16, 98, 70, 0.5)',
    'rgba(78, 42, 4, 0.5)',
];

@Component({
    selector: 'app-active-game',
    templateUrl: './range-game.component.html',
    styleUrls: ['./range-game.component.scss']
})
export class RangeGameComponent implements OnInit {

    bets: Bet[] = [];
    players: Player[] = [];
    accountPlayer: Player;
    sections: Section[] = [];
    roundTime = 40;
    roundTimeLeft: number;
    betFrom = 0;
    betTo = 100;
    betAmount = 0;
    gameId: string;

    constructor(private route: ActivatedRoute, private userService: UserService,
        private gameService: GameService, private socketService: SocketService) {
        this.route.params.subscribe(params => {
            this.gameId = params.id;
        });
    }

    async ngOnInit() {
        try {
            await this.userService.loadData();
        }
        catch (e) {
            console.log('Could not load UserService.');
            return;
        }

        this.socketService.onNewBet().subscribe(bet => {
            this.bets.push(bet);
            this.updateSections();
        });

        let game = await this.gameService.getGame(this.gameId);
        this.bets = game.currentRoundBets as Bet[];
        this.players = game.players.map(p => new Player(p.id, p.steamName, p.avatarUrl, colorsHex[0], p.coins));
        this.accountPlayer = this.players.find(p => p.id == this.userService.id);
        this.updateSections();
    }

    get betsSum() {
        return this.bets.reduce((acc, b) => acc + b.amount, 0);
    }

    get accountPlayerBets() {
        return this.bets.filter(b => b.playerId == this.accountPlayer.id);
    }

    updateSections() {
        let breakpoints: number[] = [];
        this.sections = [];

        for (let bet of this.bets) {
            if (breakpoints.indexOf(bet.from) == -1) breakpoints.push(bet.from);
            if (breakpoints.indexOf(bet.to) == -1) breakpoints.push(bet.to);
        }

        breakpoints.sort((a, b) => a - b);

        let prevSection: Section;
        for (let i = 1; i < breakpoints.length; i++) {
            let bpFrom = breakpoints[i - 1];
            let bpTo = breakpoints[i];

            let section = new Section(bpFrom, bpTo, 0);

            for (let bet of this.bets) {
                if ((bet.from <= bpFrom && bet.to > bpFrom)
                    || (bet.from > bpFrom && bet.from < bpTo)) {
                    section.amount += bet.amount;
                }
            }

            // Merge unnecessary sections
            if (prevSection && prevSection.to == section.from && prevSection.amount == section.amount) {
                prevSection.to = section.to;
            } else if (section.amount != 0) {
                this.sections.push(section);
                prevSection = section;
            }
        }
    }

    async placeBet() {
        try {
            let bet = await this.gameService.placeBet(this.gameId, this.betFrom, this.betTo, this.betAmount) as Bet;
        }
        catch (e) {
            console.log(e);
        }
    }
}
