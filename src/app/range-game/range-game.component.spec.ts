import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RangeGameComponent } from './range-game.component';

describe('ActiveGameComponent', () => {
  let component: RangeGameComponent;
  let fixture: ComponentFixture<RangeGameComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RangeGameComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RangeGameComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
