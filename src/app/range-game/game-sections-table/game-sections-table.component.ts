import { Component, OnInit, Input } from '@angular/core';
import { Section } from '../../models/section';
import { UtilService } from '../../util/util.service';

@Component({
    selector: 'app-game-sections-table',
    templateUrl: './game-sections-table.component.html',
    styleUrls: ['./game-sections-table.component.scss']
})
export class GameSectionsTableComponent implements OnInit {

    @Input() sections: Section[];
    @Input() betsSum: number;

    constructor(public utilService: UtilService) { }

    ngOnInit() {
    }

    get maxSection(): number {
        return this.sections.reduce((a, b) => a.amount > b.amount ? a : b).amount;
    }
}
