import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GameSectionsTableComponent } from './game-sections-table.component';

describe('GameSectionsTableComponent', () => {
  let component: GameSectionsTableComponent;
  let fixture: ComponentFixture<GameSectionsTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GameSectionsTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GameSectionsTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
