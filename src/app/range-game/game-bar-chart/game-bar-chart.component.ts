import { Component, OnInit, Input, IterableDiffer, IterableDiffers, DoCheck } from '@angular/core';
import { Section } from '../../models/section';
import { UtilService } from '../../util/util.service';

@Component({
    selector: 'app-game-bar-chart',
    templateUrl: './game-bar-chart.component.html',
    styleUrls: ['./game-bar-chart.component.scss']
})
export class GameBarChartComponent implements OnInit {

    @Input() sections: Section[];
    @Input() betsSum: number;

    sectionsDiff: IterableDiffer<Section>;

    constructor(private iterableDiffers: IterableDiffers, public utilService: UtilService) {
        this.sectionsDiff = iterableDiffers.find([]).create(null);
    }

    get maxSection(): number {
        return this.sections.reduce((a,  b) => a.amount > b.amount ? a : b).amount;
    }

    get minSection(): number {
        return this.sections.reduce((a,  b) => a.amount > b.amount ? b : a).amount;
    }

    get breakpoints() {
        let breakpoints: number[] = [];

        for (let section of this.sections) {
            if (breakpoints.indexOf(section.from) == -1) breakpoints.push(section.from);
            if (breakpoints.indexOf(section.to) == -1) breakpoints.push(section.to);
        }

        return breakpoints;
    }

    ngOnInit() {
        this.updateSections();
        console.log(this.sections);
    }

    ngDoCheck() {
        let sectionsChanged = this.sectionsDiff.diff(this.sections);
        if (sectionsChanged) {
            this.updateSections();
        }
    }

    updateSections() {

    }
}
