import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GameBarChartComponent } from './game-bar-chart.component';

describe('GameBarChartComponent', () => {
  let component: GameBarChartComponent;
  let fixture: ComponentFixture<GameBarChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GameBarChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GameBarChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
