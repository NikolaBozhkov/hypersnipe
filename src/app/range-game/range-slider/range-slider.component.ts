import { Component, OnInit, Input, Output, ViewChild, ElementRef, HostListener, EventEmitter } from '@angular/core';

@Component({
    selector: 'app-range-slider',
    templateUrl: './range-slider.component.html',
    styleUrls: ['./range-slider.component.scss']
})
export class RangeSliderComponent implements OnInit {

    @Input() min: number;
    @Input() max: number;
    @Input() step: number;
    @Input() minRange: number;

    private _from: number;
    private _to: number;

    @Output() fromChange = new EventEmitter();
    @Output() toChange = new EventEmitter();

    @ViewChild('thumbFrom') thumbFrom: ElementRef;
    @ViewChild('thumbTo') thumbTo: ElementRef;
    @ViewChild('track') track: ElementRef;
    @ViewChild('labelMin') labelMin: ElementRef;
    @ViewChild('labelMax') labelMax: ElementRef;
    @ViewChild('labelLow') labelLow: ElementRef;
    @ViewChild('labelHigh') labelHigh: ElementRef;

    selectedThumb: HTMLElement;
    isThumbActive = false;
    isRangeActive = false;
    isRangeSelected = false;
    lastMouseX: number;
    rangeDeltaValue: number = 0;

    isLabelMinHidden: boolean;
    isLabelMaxHidden: boolean;

    constructor() { }

    @Input()
    get from() {
        return this._from;
    }

    set from(from: number) {
        this._from = from;
        this.updateMinLabel();
        this.fromChange.emit(this._from);
    }

    @Input()
    get to() {
        return this._to;
    }

    set to(to: number) {
        this._to = to;
        this.updateMaxLabel();
        this.toChange.emit(this._to);
    }

    get fromPercent(): number {
        return (this.from - this.min) / (this.max - this.min) * 100;
    }

    get toPercent(): number {
        return (this.to - this.min) / (this.max - this.min) * 100;
    }

    get multiplier(): number {
        return 100 / (this.to - this.from);
    }

    ngOnInit() {
    }

    selectThumb(thumb: HTMLElement) {
        this.selectedThumb = thumb;
    }

    deselectThumb() {
        this.selectedThumb = null;
    }

    activateThumb(event: MouseEvent, thumb: HTMLElement) {
        event.preventDefault();
        thumb.focus();
        this.isThumbActive = true;
    }

    selectRange() {
        this.isRangeSelected = true;
    }

    deselectRange() {
        this.isRangeSelected = false;
    }

    activateRange(event: MouseEvent) {
        event.preventDefault();
        (event.target as HTMLElement).focus();
        this.lastMouseX = event.clientX
            - (this.track.nativeElement as HTMLElement).getBoundingClientRect().left;
        this.isRangeActive = true;
    }

    @HostListener('window:mouseup')
    deactivateAll() {
        this.isThumbActive = false;
        this.isRangeActive = false;
    }

    @HostListener('window:mousemove', ['$event'])
    move(event: MouseEvent) {
        if (this.isThumbActive || this.isRangeActive) {
            let valuesChanged = false;
            let boundingRect = (this.track.nativeElement as HTMLElement).getBoundingClientRect();
            let xPos = event.clientX - boundingRect.left;

            if (this.isThumbActive) {

                // Clamp position
                xPos = xPos < 0 ? 0 : xPos > boundingRect.width ? boundingRect.width : xPos;
                let value = Math.floor(this.getValueByPercent(xPos / boundingRect.width));

                // Change appropriate value and limit to bounds
                if (this.selectedThumb === this.thumbFrom.nativeElement) {
                    if (value > this.to - this.minRange) {
                        value = this.to - this.minRange;
                    }

                    this.from = value;
                } else {
                    if (value < this.from + this.minRange) {
                        value = this.from + this.minRange;
                    }

                    this.to = value;
                }
            } else if (this.isRangeActive) {
                let deltaX = xPos - this.lastMouseX;
                let deltaValue = this.getValueByPercent(deltaX / boundingRect.width);

                // Accumulate the delta if mouse movements are really small
                this.rangeDeltaValue += deltaValue;
                deltaValue = Math.round(this.rangeDeltaValue);

                if (Math.abs(deltaValue) >= this.step) {
                    this.rangeDeltaValue -= deltaValue;

                    // Check if both low and high bound can be moved
                    if (this.from + deltaValue >= this.min && this.to + deltaValue <= this.max) {
                        this.from += deltaValue;
                        this.to += deltaValue;
                    }
                }

                this.lastMouseX = xPos;
            }
        }
    }

    getValueByPercent(percent: number) {
        return this.min + (this.max - this.min) * percent;
    }

    updateMinLabel() {
        let trackWidth = (this.track.nativeElement as HTMLElement).offsetWidth;
        let availableWidthMin = trackWidth * this.fromPercent / 100
            - (this.labelLow.nativeElement as HTMLElement).offsetWidth / 2;
        this.isLabelMinHidden = availableWidthMin <= (this.labelMin.nativeElement as HTMLElement).offsetWidth;
    }

    updateMaxLabel() {
        let trackWidth = (this.track.nativeElement as HTMLElement).offsetWidth;
        let availableWidthMax = trackWidth - trackWidth * this.toPercent / 100
            - (this.labelHigh.nativeElement as HTMLElement).offsetWidth / 2;
        this.isLabelMaxHidden = availableWidthMax <= (this.labelMax.nativeElement as HTMLElement).offsetWidth;
    }
}
