import { Injectable } from '@angular/core';
import { Constants } from './constants';
import { Section } from '../models/section';

@Injectable()
export class UtilService {

    constructor() { }

    public getSectionColor(section: Section, sections: Section[], alpha: number = 1): string {
        let maxSection = sections.reduce((a, b) => a.amount > b.amount ? a : b).amount;
        let minSection = sections.reduce((a, b) => a.amount > b.amount ? b : a).amount;

        let percent: number;
        if (maxSection == section.amount) {
            percent = 1;
        } else {
            percent = (section.amount - minSection) / (maxSection - minSection);
        }

        return this.colorAtPercent(Constants.ColorHighBet, Constants.ColorLowBet, percent, alpha);
    }

    public colorAtPercent(color1: string, color2: string, percent: number, alpha = 1): string {
    	var r = Math.ceil(parseInt(color1.substring(1, 3), 16) * percent + parseInt(color2.substring(1, 3), 16) * (1 - percent));
    	var g = Math.ceil(parseInt(color1.substring(3, 5), 16) * percent + parseInt(color2.substring(3, 5), 16) * (1 - percent));
    	var b = Math.ceil(parseInt(color1.substring(5, 7), 16) * percent + parseInt(color2.substring(5, 7), 16) * (1 - percent));

    	return `rgba(${r}, ${g}, ${b}, ${alpha})`;
    }
}
