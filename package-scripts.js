const {series, crossEnv, concurrent, rimraf} = require('nps-utils')
const {config: {port : E2E_PORT}} = require('./protractor.conf')

module.exports = {
  scripts: {
	default: 'concurrently --kill-others "nps ng.serve" "nps babel.node"',
	build: 'nps webpack.build',
	babel: {
		node: 'nodemon server/server.js --exec babel-node'
	},
    ng: {
        default: 'ng',
        serve: 'ng serve --proxy-config proxy.config.json',
        build: 'ng build'
    },
    e2e: 'ng e2e',
    test: 'ng test',
    lint: 'ng lint'
  },
}
