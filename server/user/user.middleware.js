import User from './user.model';

export default {
    async getUser(req, res, next) {
		try {
            let user = await User.findById(req.user.id);
			req.user = user;
			next();
        } catch (err) {
            return res.status(500).json({ err });
        }
	}
}
