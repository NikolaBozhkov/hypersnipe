import { matchedData } from 'express-validator/filter';

import config from '../config/config';
import User from './user.model';

export default {
	async getUser(req, res) {
		return res.json(req.user);
	},

	updateReferralInformation() {

	},

	async updateTradeUrl(req, res) {
		let tradeUrl = matchedData(req).tradeUrl;

		try {
			await User.findOneAndUpdate({
				_id: req.user.id
			}, {
				tradeUrl: tradeUrl
			});

			return res.sendStatus(204);
		} catch (err) {
			console.log(err);
			return res.status(500).json(err);
		}
	},

	async getUserCurrency(req, res) {
		let currencyInfo = {
			tickets: user.tickets,
			skinCurrency: user.skinCurrency,
			ticketCurrency: user.ticketCurrency
		};

		return res.json(currencyInfo);
	}
}
