import express from 'express';
import { check } from 'express-validator/check';

import userController from './user.controller';
import authController from '../auth/auth.controller';
import userMiddleware from './user.middleware';
import authMiddleware from '../auth/auth.middleware';
import middleware from '../config/middleware';

let router = express.Router();

// /api/user
router.use(authMiddleware.isAuthenticated);

router.get('', userMiddleware.getUser, userController.getUser);
router.put('/tradeUrl', [
    check('tradeUrl').exists(),
    middleware.validationErrors], userController.updateTradeUrl);
router.get('/currency', userMiddleware.getUser, userController.getUserCurrency);

export default router;
