import jwt from 'jsonwebtoken';
import config from '../config/config';
import User from '../user/user.model';

export default {
	steamSuccessfullyAuthenticated(req, res) {
		// Successful authentication, redirect home.
		var token = jwt.sign({ id: req.user.id }, config.secret, {
			expiresIn: '3d'
		});

		// Expire(max age) in 3d
		res.cookie('token', token, { maxAge: 60000 * 60 * 24 * 3, httpOnly: false, secure: false });
		res.redirect('/');
	},

	async login(req, res) {
		try {
			let user = await User.findOne({ steamId: '76561198061679866' });
			req.user = user;
			exports.default.steamSuccessfullyAuthenticated(req, res);
		}
		catch (e) {
			console.log(e);
			res.status(500).json({ error: e });
		}
	}
}
