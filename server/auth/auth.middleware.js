import jwt from 'jsonwebtoken';
import config from '../config/config';

export default {
    isAuthenticated(req, res, next) {
        // check cookie, header or url parameters or post parameters for token
        var token = req.cookies.token;

        // decode token
        if (token) {
            // verifies secret and checks exp
            jwt.verify(token, config.secret, function (err, decoded) {
                if (err) {
                    return res.status(403).json({ success: false, message: 'Failed to authenticate token.' });
                } else {
                    // if everything is good, save to request for use in other routes
                    req.user = decoded;
                    next();
                }
            });

        } else {
            // if there is no token return an error
            return res.status(403).json({
                success: false,
                message: 'No token provided.'
            });
        }
    }
}
