import { validationResult } from 'express-validator/check';

export default {
    validationErrors(req, res, next) {
        let errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(422).json({ errors: errors.mapped() });
        }

        next();
    }
}
