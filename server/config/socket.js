import express from 'express';
import http from 'http';
import socketIo from 'socket.io';
import config from './config';
import gamesController from '../games/games.controller';

class Socket {
	constructor() {
		this.io = null;
		this.socketsByUserIdsMap = new Map();
		this.userIdsBySocketIdsMap = new Map();
		this.onlineUsersCount = 0;
	}

	configSocket(server) {
		this.io = socketIo(server);
		this.io.set("origins", "*:*");

		this.io.on('connection', (socket) => {

	        socket.on('userId', (data) => {
				if (!this.socketsByUserIdsMap.has(data.userId)) {
					this.onlineUsersCount += 1;
					this.socketsByUserIdsMap.set(data.userId, []);
					this.io.emit('onlineUsersCount', this.onlineUsersCount);
				}

				this.socketsByUserIdsMap.get(data.userId).push(socket);
				this.userIdsBySocketIdsMap.set(socket.id, data.userId);

				// Add player to game room if in game
				if (gamesController.gameIdsByUserIdsMap.has(data.userId)) {
					socket.join(gamesController.gameIdsByUserIdsMap.get(data.userId));
				}
	        });

	        socket.on('disconnect', () => {
	            let userId = this.userIdsBySocketIdsMap.get(socket.id);
				this.userIdsBySocketIdsMap.delete(socket.id);

				// Remove socket record from user id map if any
				if (this.socketsByUserIdsMap.has(userId)) {
					let indexOfSocket = this.socketsByUserIdsMap.get(userId).indexOf(socket);
					if (indexOfSocket != -1) {
						this.socketsByUserIdsMap.get(userId).splice(indexOfSocket, 1);

						// Remove the entry if all sockets have been disconnected
						if (this.socketsByUserIdsMap.get(userId).length == 0) {
							this.socketsByUserIdsMap.delete(userId);
							this.onlineUsersCount -= 1;
							this.io.emit('onlineUsersCount', this.onlineUsersCount);
						}
					}
				}
	        });
	    });
	}

	addUserToRoom(userId, roomId) {
		if (this.socketsByUserIdsMap.has(userId)) {
			this.socketsByUserIdsMap.get(userId).forEach(s => {
				s.join(roomId, () => {
					console.log('joined');
				});
			});
		}
	}
}

export let socket = new Socket();
