import passport from 'passport';
import config from './config';
import User from '../user/user.model';
import { Strategy as SteamStrategy } from 'passport-steam';

export default function() {
    // Steam auth
    passport.use(new SteamStrategy({
        returnURL:  'http://localhost:4200/api/auth/steam/return',
        realm:  'http://localhost:4200/',
        apiKey: '621C617B79E2CFA47496F59D2E40C1B4',
        passReqToCallback: true
    },
    async function(req, identifier, profile, done) {
        console.log('s');
		var steamUser = profile._json;

		// get the user from the database
		try {
			let user = await User.findOne({ steamId: steamUser.steamid });

			if (user) {
				// Update steam profile info
				user.steamName = steamUser.personaname;
				user.avatarUrl = steamUser.avatar;
				await user.save();
				return done(null, user);

			} else {
				// Create new user
				let newUser = await User.create({
					steamName: steamUser.personaname,
					steamId: steamUser.steamid,
					avatarUrl: steamUser.avatar,
					referrer: req.cookies.refId
				});

				return done(null, newUser);
			}
		} catch (err) {
			console.log(err);
			return done(err, null);
		}
    }));

    passport.serializeUser(async (user, done) => {
        if (user) {
            return done(null, user._id);
        }
    });

    passport.deserializeUser(async (id, done) => {
		try {
			let user = await User.findById(id);
			return done(null, user);
		} catch (err) {
			return done(err, null);
		}
    });
};
