import express from 'express';
import shortid from 'shortid';
import { check } from 'express-validator/check';

import authMiddleware from '../auth/auth.middleware';
import queueController from './queue.controller';
import queueMiddleware from './queue.middleware';
import userMiddleware from '../user/user.middleware';
import middleware from '../config/middleware';
import { GameType } from '../../shared/game/game-type';

let router = express.Router();

// /api/queue
router.use(authMiddleware.isAuthenticated);
router.use(userMiddleware.getUser);

router.post('', [
    queueMiddleware.gameTypeValidator,
    middleware.validationErrors,
    queueMiddleware.checkUserInGameOrQueue], queueController.queueUser);

router.post('/cancel', queueController.removeFromQueue);

router.post('/accept', [
    queueMiddleware.readyCheckIdValidator,
    middleware.validationErrors,
    queueMiddleware.checkUserInReadyCheck,
    queueMiddleware.checkReadyCheckAccepted], queueController.acceptGame);

router.post('/decline', [
    queueMiddleware.readyCheckIdValidator,
    middleware.validationErrors,
    queueMiddleware.checkUserInReadyCheck], queueController.declineGame);

// dev
router.post('/skip', queueController.skipQueue);

export default router;
