import shortid from 'shortid';
import { matchedData } from 'express-validator/filter';

import User from '../user/user.model';
import gamesController from '../games/games.controller';
import { socket } from '../config/socket';
import { ReadyCheck } from './ready-check';
import { GameType } from '../../shared/game/game-type';

const playerCount = 10;

let readyCheckByIdMap = new Map();

let queuedUserIds = [
    '5acf80bb734d1d55c31b1917',
    '5acf80d0734d1d55c31b1926',
    '5acf80e4734d1d55c31b1933',
    '5acf8108734d1d55c31b1952',
    '5acf811b734d1d55c31b1963',
    '5acf812e734d1d55c31b1965',
    '5acf813c734d1d55c31b196b',
    '5acf8150734d1d55c31b1978',
    '5acf816a734d1d55c31b1984'];

function sendGameReadyCheck(userIds, readyCheckId, gameType) {
    let readyCheck = new ReadyCheck(readyCheckId, userIds, gameType);
    readyCheckByIdMap.set(readyCheckId, readyCheck);

    for (let userId of userIds) {
        if (socket.socketsByUserIdsMap.has(userId)) {
            let sockets = socket.socketsByUserIdsMap.get(userId);
            sockets.forEach(s => s.join(readyCheckId));
        }
    }

    socket.io.to(readyCheckId).emit('gameReady', {
        playersCount: gameType.playersCount,
        readyCheckId: readyCheckId
    });
};

export default {
    readyCheckByIdMap,
    queuedUserIds,

    async queueUser(req, res) {
        // TODO: Validate enough tickets to enter queue

        // Add to queue
        queuedUserIds.push(req.user.id);

        // Check if enough for a new game (depends on game type)
        if (queuedUserIds.length >= req.body.gameType.playersCount) {
            let userIds = queuedUserIds.slice(0, req.body.gameType.playersCount);
            let readyCheckId = shortid.generate();
            sendGameReadyCheck(userIds, readyCheckId, req.body.gameType);
        }

        // TODO: Calculate estimatedTime
        return res.json({ estimatedTime: 1 });
    },

    async removeFromQueue(req, res) {
        let index = queuedUserIds.indexOf(req.user.id);
        if (index > -1) {
            queuedUserIds.splice(index, 1);
            return res.json(true);
        }

        return res.json(false);
    },

    async acceptGame(req, res) {
        let readyCheckId = req.body.readyCheckId;
        let readyCheck = readyCheckByIdMap.get(readyCheckId);
        readyCheck.setReady(req.user.id, true);
        socket.io.to(readyCheckId).emit('userAccepted');

        // Create game on enough players joined
        if (readyCheck.isReady) {
            gamesController.createGame(readyCheck.userIds);
        }

        return res.json(true);
    },

    async declineGame(req, res) {
        let readyCheckId = req.body.readyCheckId;
        let readyCheck = readyCheckByIdMap.get(readyCheckId);

        socket.io.to(readyCheckId).emit('userDeclined');

        // Destroy ready check and leave room
        for (let userId of readyCheck.userIds) {
            if (socket.socketsByUserIdsMap.has(userId)) {
                let sockets = socket.socketsByUserIdsMap.get(userId);
                sockets.forEach(s => s.leave(readyCheckId));
            }
        }

        readyCheckByIdMap.delete(readyCheckId);

        return res.json(true);
    },

    async skipQueue(req, res) {
        sendGameReadyCheck();
    }
}
