import { check } from 'express-validator/check';
import { matchedData } from 'express-validator/filter';

import queueController from './queue.controller';
import gamesController from '../games/games.controller';

import { GameType } from '../../shared/game/game-type.js';

export default {
    get readyCheckIdValidator() {
        return check('readyCheckId').custom(readyCheckId => {
            if (!queueController.readyCheckByIdMap.has(readyCheckId)) {
                return Promise.reject('The ready check id specified could not be found.');
            }

            return Promise.resolve(readyCheckId);
        });
    },

    get gameTypeValidator() {
        return check('gameType').custom(value => {
            let gameTypesJson = Object.values(GameType).map(gt => JSON.stringify(gt));

            if (gameTypesJson.indexOf(JSON.stringify(value)) == -1) {
                return Promise.reject('GameType is invalid.');
            }

            return Promise.resolve(value);
        });
    },

    checkUserInGameOrQueue(req, res, next) {
        // Check if user is not already queued or in a game
        if (queueController.queuedUserIds.indexOf(req.user.id) > -1
            || gamesController.gameIdsByUserIdsMap.has(req.user.id)) {
            return res.status(400).json({ error: 'User is already in the queue or in a game.' });
        }

        next();
    },

    checkUserInReadyCheck(req, res, next) {
        let readyCheck = queueController.readyCheckByIdMap.get(req.body.readyCheckId);
        if (!readyCheck.userIds.indexOf(req.user.id) == -1) {
            return res.status(400).json({ error: 'User not in ready check.' });
        }

        next();
    },

    checkReadyCheckAccepted(req, res, next) {
        let readyCheck = queueController.readyCheckByIdMap.get(req.body.readyCheckId);
        if (readyCheck.isUserReady(req.user.id)) {
            return res.status(304).json({ message: 'Already accepted.' });
        }

        next();
    }
}
