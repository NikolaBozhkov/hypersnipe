export class ReadyCheck {
    constructor(readyCheckId, userIds, gameType) {
        this.readyCheckId = readyCheckId;
        this.userIds = userIds;
        this.gameType = gameType;
        this.userIdToAcceptedMap = new Map(userIds.map(uid => [uid, false]));
    }

    get isReady() {
        return true;
        return Array.from(this.userIdToAcceptedMap.values()).indexOf(false) == -1;
    }

    isUserReady(userId) {
        return this.userIdToAcceptedMap.get(userId);
    }

    setReady(userId, isReady) {
        this.userIdToAcceptedMap.set(userId, isReady);
    }
}
