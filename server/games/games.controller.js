import shortid from 'shortid';
import User from '../user/user.model';
import { Player } from './player';
import { Game } from './game';
import { socket } from '../config/socket';

let gamesByIdsMap = new Map();
let gameIdsByUserIdsMap = new Map();

export default {
	gamesByIdsMap,
	gameIdsByUserIdsMap,

	async createGame(userIds) {
		try {
			let users = await User.find({
				_id: {
					$in: userIds
				}
			});

			// Validate that all users have been found
			if (userIds.length != users.length) {
				return res.status(400).json({ err: 'Not all users have been found.' });
			}

			let players = users.map(u => new Player(u.id, u.steamName, u.avatarUrl));
			let gameId = shortid.generate(); // obv change that lol

			// Save each user's game for future fast look up
			players.forEach(p => {
				gameIdsByUserIdsMap.set(p.id, gameId);
				socket.addUserToRoom(p.id, gameId);
			});

			let game = new Game(gameId, players);
			gamesByIdsMap.set(gameId, game);

			socket.io.to(gameId).emit('joinedGame', { gameId });
		} catch (err) {
			console.log(err);
		}
	},

	async getGame(req, res) {
		if (gamesByIdsMap.has(req.params.gameId)) {
			let game = gamesByIdsMap.get(req.params.gameId);
			return res.json({
				id: game.id,
				players: Array.from(game.playersByIdsMap.values()),
				currentRoundBets: game.currentRound.bets,
				roundTimeLeft: game.roundTimeLeft
			});
		} else {
			return res.status(404).json({ err: `Game with id: ${req.params.gameId} does not exist` });
		}
	},

	async createBet(req, res) {
		// Validate body
		if (req.body.from == undefined || req.body.to == undefined || req.body.amount == undefined) {
			return res.status(400).json({ err: 'Invalid body. Arguments missing.' });
		}

		// Validate range
		if (req.body.from >= req.body.to
		|| req.body.from < 0 || req.body.from >= 100
		|| req.body.to <= 0 || req.body.to > 100) {
			return res.status(400).json({ err: 'Invalid range.' });
		}

		// Validate amount
		if (req.body.amount <= 0) {
			return res.status(400).json({ err: 'Invalid bet amount. Must be greater than 0.' });
		}

		try {
			let user = await User.findById(req.user.id);

			// Get the game of the user
			let gameId = gameIdsByUserIdsMap.get(user.id);
			if (gameId == undefined) {
				return res.status(400).json({ err: 'User is not in a game.' });
			}

			let game = gamesByIdsMap.get(gameId);
			if (game == undefined) {
				// This should be impossible
			}

			// Get the player object of the user
			let player = game.playersByIdsMap.get(user.id);
			if (player == undefined) {
				// This should be impossible
				return res.status(500).json({ err: 'Player is not in the game.' });
			}

			// Validate bet amount
			if (player.coins < req.body.amount) {
				return res.status(400).json({ err: 'Not enough coins.' });
			}

			// Validate round roundTimeLeft
			if (game.roundTimeLeft <= 0) {
				return res.status(400).json({ err: 'The round has ended.' });
			}

			let bet = game.placeBet(req.body.from, req.body.to, req.body.amount, player);
			if (bet) {
				socket.io.to(gameId).emit('newBet', bet);
				return res.json(bet);
			} else {
				console.log('bet not created');
				return res.status(400).json({err: 'The bet could not be placed.'});
			}
		} catch (err) {
			console.log(err);
			return res.status(500).json({ err });
		}
	},

	// dev
	leaveGame(req, res) {
		let gameId = gameIdsByUserIdsMap.get(req.user.id);
		if (gameId) gameIdsByUserIdsMap.delete(req.user.id);

		let game = gamesByIdsMap.get(gameId);
		if (game) game.playersByIdsMap.delete(req.user.id);

		return res.json(true);
	}
};
