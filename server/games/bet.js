export class Bet {
    constructor(from, to, amount, playerId) {
        this.from = from;
        this.to = to;
        this.amount = amount;
        this.playerId = playerId;
        this.multiplier = 100 / (to - from);
        this.winAmount = this.multiplier * amount;
    }
}
