import express from 'express';
import gamesController from './games.controller';
import authMiddleware from '../auth/auth.middleware';
const router = express.Router();

// /api/games
router.get('/:gameId', authMiddleware.isAuthenticated, gamesController.getGame);
router.post('/:gameId/bet', authMiddleware.isAuthenticated, gamesController.createBet);

// dev
router.post('/leave', authMiddleware.isAuthenticated, gamesController.leaveGame);

export default router;
