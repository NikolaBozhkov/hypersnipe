const STARTING_AMOUNT = 100000;

export class Player {
    constructor(id, steamName, avatarUrl) {
        this.id = id;
        this.steamName = steamName;
        this.avatarUrl = avatarUrl;
        this.coins = STARTING_AMOUNT;
        this.won = 0;
        this.lost = 0;
    }
}
