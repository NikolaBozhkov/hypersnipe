import { Bet } from './bet';

export class Round {
    constructor() {
        this.bets = [];
        this.didFinish = false;
    }

    get amountBets() {
        return this.bets.reduce((b, acc) => b.amount + acc, 0);
    }
}
