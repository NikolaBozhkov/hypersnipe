import { Player } from './player';
import { Round } from './round';
import { Bet } from './bet';

// Contains Rounds, manages incoming bets(routes them to the right round)
// Manages all top level logic of the game
export class Game {

    constructor(id, players) {
        this.id = id;
        this.playersByIdsMap = new Map(players.map(p => [p.id, p]));
        this.rounds = [];
        this.roundTimeLeft = 40;

        this.currentRound = new Round();
    }

    placeBet(from, to, amount, player) {
        if (!this.playersByIdsMap.has(player.id)) {
            console.log('Player is not in the game');
            return null;
        }

        if (player.coins < amount) {
            console.log('Player does not have enough coins');
            return null;
        }

        player.coins -= amount;
        let bet = new Bet(from, to, amount, player.id);
        this.currentRound.bets.push(bet);
        return bet;
    }

    endRound() {
        let sumAmount = this.currentRound.amountBets;

        // TODO: Provably fair random generation
        let random = Math.floor(Math.random()) * 101;

        let wonBets = bets.filter(b => b.from <= random && random <= b.to);
        let wonAmount = wonBets.reduce((acc, w) => acc + w.winAmount, 0);

        let distributionCoef = Math.min(sumAmount / wonAmount, 1);

        for (let wonBet of wonBets) {
            let player = this.playersByIdsMap.get(wonBet.playerId);
            player.coins += distributionCoef * wonBet.winAmount;
        }

        // Need to return
        let returnAmount = sumAmount - wonAmount;
        if (returnAmount > 0) {
            let lostBets = bets.filter(b => !(b.from <= random && random <= b.to));
            let lostBetsSum = lostBets.reduce((acc, l) => acc + l.amount, 0);
            let returnCoef = returnAmount / lostBetsSum;

            for (let lostBet of lostBets) {
                let player = this.playersByIdsMap.get(lostBet.playerId);
                player.coins += returnCoef * lostBet.amount;
            }
        }

        this.currentRound.didFinish = true;
        this.rounds.push(this.currentRound);
        this.currentRound = new Round();

        // TODO: Notify clients that the round has ended and send rolled number
        // Decide whether to send full data or recalculate everything on the client
    }
}
