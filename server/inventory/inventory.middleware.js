import { matchedData } from 'express-validator/filter';

import { Item, items } from './items';
import { constants, itemType, itemsUtil } from '../../shared/util';

export default {
    getItem(req, res, next) {
        let data = matchedData(req);
        let index = req.user.inventory.findIndex(
            item => item.type == data.type
            && item.name == data.name);

        if (index == -1) {
            return res.status(400).json({ error: 'The item is not in the user inventory.' });
        }

        req.body.item = req.user.inventory[index];
        req.body.itemIndex = index;
        next();
    },

    getBlueprintUpgradeCost(req, res, next) {
        let item = req.body.item;
		let itemPrice = items[item.name].price;

		// Calculate upgrade price
		let upgradeCost = itemPrice * constants.blueprintUpgradeRatio;

		// Validate if user has enough money
		if (req.user.skinCurrency < upgradeCost) {
			return res.status(400).json({ error: 'Not enough skin currency to upgrade blueprint.' });
		}

        req.body.upgradeCost = upgradeCost;
        next();
    }
}
