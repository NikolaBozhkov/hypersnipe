import express from 'express';
import { check } from 'express-validator/check';
import { matchedData } from 'express-validator/filter';

import authMiddleware from '../auth/auth.middleware';
import userMiddleware from '../user/user.middleware';
import middleware from '../config/middleware';
import inventoryController from './inventory.controller';
import inventoryMiddleware from './inventory.middleware';
import { itemType } from '../../shared/util';

let router = express.Router();

router.use(authMiddleware.isAuthenticated);
router.use(userMiddleware.getUser);

// api/inventory
router.get('', inventoryController.getInventory);

router.put('/sell', [
    check('name').exists(),
    check('type').exists(),
    inventoryMiddleware.getItem,
    middleware.validationErrors], inventoryController.sellItem);

router.put('/upgrade', [
    check('name').exists(),
    check('type').equals(itemType.skinBlueprint),
    inventoryMiddleware.getItem,
    inventoryMiddleware.getBlueprintUpgradeCost,
    middleware.validationErrors], inventoryController.upgradeSkinBlueprint);

router.put('/forge', middleware.validationErrors, inventoryController.forgeItems)

// FOR TESTING
router.post('/addItem', inventoryController.addItem);
router.post('/removeItem', inventoryController.removeItem);
router.get('/allItems', inventoryController.getAllItems);

export default router;
