import fs from 'fs';
import { itemType } from '../../shared/util';

export class Item {
	constructor(hashName, type, meta) {
		this.hashName = hashName;
		this.type = type;
		this.name = meta.name;
		this.iconUrl = meta.iconUrl;
		this.nameColor = meta.nameColor;
		this.qualityColor = meta.qualityColor;
		this.price = meta.price;
	}
}

export let items = {};

export function loadItems() {
	let itemsString = fs.readFileSync(__dirname + '/items.txt', 'utf8');
	let itemsPricesString = fs.readFileSync(__dirname + '/items-prices.txt', 'utf8');

	let itemsPrices = JSON.parse(itemsPricesString).response.items;

	for (let item of JSON.parse(itemsString).items) {
		let hashName = item['market_hash_name'];
		let itemPrice = itemsPrices[hashName];

		items[hashName] = new Item(hashName, itemType.raw, {
			name: item['market_name'],
			iconUrl: item['icon_url'],
			nameColor: item['name_color'],
			qualityColor: item['quality_color'],
			price: itemPrice && itemPrice.value || -1
		});
	}
}
