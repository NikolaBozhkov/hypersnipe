import { matchedData } from 'express-validator/filter';

import User from '../user/user.model';
import { Item, items } from './items';
import { constants, itemType, itemsUtil } from '../../shared/util';
import forge from './forge';
import fs from 'fs';

export default {
	async getInventory(req, res) {
		let returnInventory = [];

		// Populate with item information
		for (let item of req.user.inventory) {
			returnInventory.push(new Item(item.name, item.type, items[item.name]));
		}

		return res.json(returnInventory);
	},

	async sellItem(req, res) {
		let item = req.body.item;
		let itemPrice = items[item.name].price;
		let sellPrice = itemsUtil.getSellPrice(item.type, itemPrice);

		// Update user info and save
		req.user.skinCurrency += sellPrice;
		req.user.inventory.splice(req.body.itemIndex, 1);

		try {
			await req.user.save();
		}
		catch (e) {
			return res.status(500).json(err);
		}

		return res.json({ item, sellPrice });
	},

	async upgradeSkinBlueprint(req, res) {
		// Update user info and save
		req.user.skinCurrency -= req.body.upgradeCost;
		req.user.inventory[req.body.itemIndex].type = itemType.skinTicket;
		await req.user.save();

		return res.json({ item, upgradeCost: req.body.upgradeCost });
	},

	async forgeItems(req, res) {
		// Validate body contains items
		if (!req.body.items) {
			return res.status(400).json({ error: 'Body must contain "items" field.' });
		}

		try {
			let user = await User.findById(req.user.id);

			// Validate items are in inventory
			for (let item of req.body.items) {
				let index = user.inventory.findIndex(i => i.name == item.name);
				if (index == -1) {
					return res.status(400).json({ error: 'Some of the items are not in the inventory.' });
				}
			}

			// TODO: Validate if selected items are forgable together

			// Get resulting item of the forge
			let resultItem = forge.forgeItems(req.body.items);

			// Update user inventory
			user.inventory = user.inventory.filter(i => {
				// filter by items that are not contained in the req body
				let index = req.body.items.findIndex(toRemove => toRemove.name == i.name);
				return index == -1;
			});

			user.inventory.push(resultItem);
			await user.save();

			return res.json(resultItem);
		} catch (err) {
			console.log(err);
			return res.status(500).json(err);
		}
	},

	// FOR TESTING PURPOSES
	async addItem(req, res) {
		try {
			let user = await User.findById(req.user.id);

			user.inventory.push({
				name: req.body.item.name,
				type: req.body.item.type
			});

			await user.save();
			return res.json(req.body.item);
		} catch (err) {
			console.log(err);
			return res.status(500).json(err);
		}
	},

	async removeItem(req, res) {
		try {
			let user = await User.findById(req.user.id);

			let index = user.inventory.findIndex(i =>
				i.name == req.body.item.name
				&& i.type == req.body.item.type
			);

            if (index > -1) {
                user.inventory.splice(index, 1);
            }

			await user.save();
			return res.json(req.body.item);
		} catch (err) {
			console.log(err);
			return res.status(500).json(err);
		}
	},

	async getAllItems(req, res) {
		return res.json(Object.keys(items));
	}
}
