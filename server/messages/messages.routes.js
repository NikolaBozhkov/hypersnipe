import express from 'express';
import { check } from 'express-validator/check';

import messagesController from './messages.controller';
import authMiddleware from '../auth/auth.middleware';
import userMiddleware from '../user/user.middleware';
import middleware from '../config/middleware';

const router = express.Router();

// /api/messages
router.get('', messagesController.getAll);
router.post('', [
    check('text').exists(),
    authMiddleware.isAuthenticated,
    userMiddleware.getUser,
    middleware.validationErrors], messagesController.createNew);

export default router;
