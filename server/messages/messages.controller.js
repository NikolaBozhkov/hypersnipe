import { matchedData } from 'express-validator/filter';

import User from '../user/user.model';
import { Message } from '../../shared/messages/message';
import { io } from '../config/socket';

let messages = [];

export default {
	getAll(req, res) {
		return res.json(messages);
	},

	async createNew(req, res) {
		let text = matchedData(req).text;
		let msg = new Message(req.user.steamName, req.user.avatarUrl, text);
		messages.push(msg);

		io.emit('newMessage', msg);
		return res.status(201).json(msg);

	}
};
